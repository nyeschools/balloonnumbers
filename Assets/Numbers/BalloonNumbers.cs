﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "BalloonNumbers")]
public class BalloonNumbers : ScriptableObject
{
    [Serializable]
    public class Entry
    {
        public Int32 Number;
        public Sprite Sprite;
    }

    [SerializeField] private List<Entry> _Numbers;

    public Sprite GetSprite(Int32 number)
    {
        return _Numbers.FirstOrDefault(s => s.Number == number)?.Sprite;
    }
}
