﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BalloonPoppedEvent")]
public class BalloonPoppedEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<BalloonPoppedEventListener> _EventListeners =
        new List<BalloonPoppedEventListener>();

    public virtual void Raise(Boolean win, Int32 number)
    {
        for (int i = _EventListeners.Count - 1; i >= 0; i--)
        {
            if (i >= _EventListeners.Count)
            {
                continue;
            }
            _EventListeners[i].OnEventRaised(win,number);
        }
    }

    public void AddListener(BalloonPoppedEventListener listener)
    {
        if (!_EventListeners.Contains(listener))
            _EventListeners.Add(listener);
    }

    public void RemoveListener(BalloonPoppedEventListener listener)
    {
        if (_EventListeners.Contains(listener))
            _EventListeners.Remove(listener);
    }

}
