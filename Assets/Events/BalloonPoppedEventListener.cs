﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class BalloonPoppedEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public BalloonPoppedEvent Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public BalloonPoppedUnityEvent Response;

    private void OnEnable()
    {
        Event.AddListener(this);
    }

    private void OnDisable()
    {
        Event.RemoveListener(this);
    }

    public void OnEventRaised(Boolean win, Int32 number)
    {
        Response.Invoke(win, number);
    }

}

[Serializable]
public class BalloonPoppedUnityEvent : UnityEvent<Boolean, Int32>
{

}
