﻿using System;
using System.Security.Cryptography;
using UnityEngine;

public class Balloon : MonoBehaviour
{
    [SerializeField] private BalloonPoppedEvent _BalloonPoppedEvent;

    [SerializeField] private Animator _Animator;
    [SerializeField] private SpriteRenderer _NumberSprite;

    [SerializeField] private Int32 _Number;

    [SerializeField] private BalloonNumbers _PossibleNumbers;

    [SerializeField] private float _Speed;

    private void Awake()
    {
        SetNumber(_Number);
    }

    //Triggered from the animation animation last key frame
    private void OnPopped()
    {
        _BalloonPoppedEvent?.Raise(true,_Number);
        Destroy(gameObject);
    }

    private void OnMouseDown()
    {
        _Animator.SetBool("Pop", true);
        _NumberSprite.enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        _BalloonPoppedEvent?.Raise(false,_Number);
        Destroy(this.gameObject);
    }

    private void Update()
    {
        transform.position += new Vector3(0, _Speed * Time.deltaTime, 0f);
    }

    public void SetNumber(Int32 number)
    {
        _Number = number;
        _NumberSprite.sprite = _PossibleNumbers.GetSprite(number);
    }
}
