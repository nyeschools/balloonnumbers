﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class BalloonController : MonoBehaviour
{
    public int TargetNumber = 1;
    public bool TargetNumberShowing = false;

    public float ElapsedSeconds = 0;
    public float SpawnSeconds = 1.5f;

    [SerializeField] private List<GameObject> _BalloonPrefabs;

    private List<Transform> _SpawnPositions = new List<Transform>();

    private int GetNextNumber()
    {
        if (!TargetNumberShowing)
        {
            TargetNumberShowing = true;
            return TargetNumber;
        }

        List<int> numbers = new List<Int32>();
        for (int i = 1; i <= 10; i++)
        {
            numbers.Add(i);
        }

        var index = Random.Range(0, numbers.Count);
        var nextNumber = numbers[index];
        if (nextNumber == TargetNumber)
        {
            TargetNumberShowing = true;
        }
        return nextNumber;
    }

    private void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            _SpawnPositions.Add(transform.GetChild(i).transform);
        }
    }

    private void Update()
    {
        ElapsedSeconds -= Time.deltaTime;
        if (ElapsedSeconds > 0) return;
        ElapsedSeconds = SpawnSeconds;

        SpawnNewBalloon(Random.Range(0, 3), GetNextNumber());
    }

    private void SpawnNewBalloon(int index, Int32 number)
    {
        GameObject bpf = _BalloonPrefabs.First();

        GameObject balloon = GameObject.Instantiate(bpf);
        balloon.transform.position = _SpawnPositions[index].position;

        balloon.GetComponentInChildren<Balloon>()?.SetNumber(number);
    }

    public void BalloonPopped(Boolean win, Int32 number)
    {
        if (number == TargetNumber)
        {
            if (win)
            {
                TargetNumber++;
            }
            TargetNumberShowing = false;
        }
    }
}
